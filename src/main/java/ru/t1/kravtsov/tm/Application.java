package ru.t1.kravtsov.tm;

import ru.t1.kravtsov.tm.constant.ArgumentConst;
import ru.t1.kravtsov.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        parseArguments(args);
        parseCommands();
    }

    private static void parseArguments(final String[] args) {
        if (args == null || args.length == 0) return;

        final String arg = args[0];
        parseArgument(arg);
        exit();
    }

    private static void parseCommands() {
        displayWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("\nENTER COMMAND:");
            final String command = scanner.next();
            parseCommand(command);
        }
    }

    private static void parseArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;

        switch (arg) {
            case ArgumentConst.VERSION:
                displayVersion();
                break;
            case ArgumentConst.ABOUT:
                displayAbout();
                break;
            case ArgumentConst.HELP:
                displayHelp();
                break;
            default:
                displayArgumentError();
        }
    }

    private static void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;

        switch (command) {
            case TerminalConst.VERSION:
                displayVersion();
                break;
            case TerminalConst.ABOUT:
                displayAbout();
                break;
            case TerminalConst.HELP:
                displayHelp();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            default:
                displayCommandError();
        }
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Display program version.\n", TerminalConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s - Display developer info.\n", TerminalConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s - Display list of terminal commands.\n", TerminalConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s - Close application.\n", TerminalConst.EXIT);
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.4.0");
    }

    private static void displayAbout() {
        System.out.println("[DEVELOPER]");
        System.out.println("NAME: Alexander Kravtsov");
        System.out.println("EMAIL: aekravtsov@nota.tech");
    }

    private static void exit() {
        System.exit(0);
    }

    private static void displayArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported");
        System.exit(1);
    }

    private static void displayCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported");
    }

}